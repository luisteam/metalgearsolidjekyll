---
layout: post
title:  "Big Boss"
---
# El comienzo de una era

**Big Boss (1935-2014)** es el nombre clave del legendario soldado que fundó la unidad [FOXHOUND][0] de fuerzas especiales, que logró derrotar a la legendaria soldado [The Boss][1] (su mentora) y evitar varias catástrofes nucleares arriesgando su vida a toda costa por el bien del mundo sin importarle los riesgos.

También fue el fundador de [Outer Heaven][2] en Sudáfrica y fundó más adelante la nación de [Zanzibar Land][3] en Asia central. Algunos consideraban a Big Boss como el “mejor soldado del siglo XX” habiendo derrotado el solo a la [Unidad Cobra][4], a The Boss, al coronel [Volgin][5] , varias armas IA ([Pupa][6], [Cocoon][7] y [Chrysalis][8]) y armas superpoderosas como el [Shagohod][9] (aunque este logro derrotarlo con la ayuda de [EVA][10] ) el [Peace Walker][11] y el [Metal Gear ZEKE][12] , también es considerado un brillante líder militar y una gran persona haciendo que sus soldados y compañeros le cogieran un afecto especial llegando incluso a dar su vida por el.

Es un maestro en el combate [CQC][13], un estilo de lucha que el y The Boss crearon, pudiendo llegar a enfrentarse a varios enemigos a la vez y dejarlos fuera de combate en cuestion de segundos. Sus simpatizantes lo consideraban como un héroe de guerra y considerado por sus enemigos como un tirano. Como resultado del proyecto [Les Enfants Terribles][14], fue el “padre” de [Solid Snake][15] (su subordinado y entonces, Némesis) y de sus hermanos clones, [Liquid Snake][16] y Solidus Snake.

Antes de ganar el sobrenombre de Big Boss, sirvió como agente de la unidad FOX bajo el nombre en código de **Naked Snake** durante la Guerra Fría. Su verdadero nombre es John, aunque es apodado Jack.

> *"A partir de ahora, llámame Big Boss."*
> – Naked Snake


#### Big Boss


![Big Boss](/metalgearsolidestatica/img/mgsvgz_bigboss_by_georgesears1972-d6wkfxc.jpg "Big Boss en Ground Zeroes")

[0]: <http://es.metalgear.wikia.com/wiki/FOXHOUND> "Fox Hound wiki"
[1]: <http://es.metalgear.wikia.com/wiki/The_Boss> "The_Boss wiki"
[2]: <http://es.metalgear.wikia.com/wiki/Outer_Heaven> "Outer_Heaven wiki"
[3]: <http://es.metalgear.wikia.com/wiki/Zanzibar_Land> "Zanzibar_Land wiki"
[4]: <http://es.metalgear.wikia.com/wiki/Unidad_Cobra> "Unidad_Cobra wiki"
[5]: <http://es.metalgear.wikia.com/wiki/Volgin> "Volgin wiki"
[6]: <http://es.metalgear.wikia.com/wiki/Pupa> "Pupa wiki"
[7]: <http://es.metalgear.wikia.com/wiki/Cocoon> "Cocoon wiki"
[8]: <http://es.metalgear.wikia.com/wiki/Chrysalis> "Chrysalis wiki"
[9]: <http://es.metalgear.wikia.com/wiki/Shagohod> "Shagohod wiki"
[10]: <http://es.metalgear.wikia.com/wiki/EVA> "EVA wiki"
[11]: <http://es.metalgear.wikia.com/wiki/Peace_Walker> "Peace_Walker wiki"
[12]: <http://es.metalgear.wikia.com/wiki/Metal_Gear_ZEKE> "Metal_Gear_ZEKE wiki"
[13]: <http://es.metalgear.wikia.com/wiki/CQC> "CQC wiki"
[14]: <http://es.metalgear.wikia.com/wiki/Les_Enfants_Terribles> "Les_Enfants_Terribles wiki"
[15]: <http://es.metalgear.wikia.com/wiki/Solid_Snake> "Solid wiki"
[16]: <http://es.metalgear.wikia.com/wiki/Liquid_Snake> "Liquid wiki"
